# Все команды терминала для lesson4
* ``sudo -i -u postgres``
* ``psql``
1. Создание пользователя intern
    * ``create role intern with login createdb;``
    * ``\password intern`` (я указал парол: "intern")
    * ``\q``
2. Заходим под именем intern
    * ``psql -U intern -d postgres -h 127.0.0.1``
3. Создаём базу данных market и подключаемся к ней
    * ``create database market;``
    * ``\c market``
4. Создаём таблицы
    * ``create table users(
id serial primary key,
name varchar(20),
money money);``
    * ``create table products(
id serial primary key,
type varchar(10),
name varchar(20),
price money,
owner integer not null,
foreign key(owner) references users(id) on delete cascade);``
5. Заполняем таблицы данными
    * ``insert into users(name, money) values
('nikita', 1400000),
('ivan', 10000),
('marta', 49990),
('tom', 33440),
('sonik', 2),
('masha', 3000),
('vika', 90000);``
    * ``insert into products(type, name, price, owner) values
('car', 'ford', 30000, 1),
('house', 'big', 40000, 1),
('computer', 'hp', 300, 2),
('processor', 'intel', 400, 3),
('shop', 'product', 4000, 3),
('cinema', '3d', 5000, 3),
('jeans', 'disel', 30, 3),
('bike', 'suzuki', 500, 3),
('car', 'bmv', 54400, 4),
('surprise', null, null, 5),
(null, null, null, 5),
('airplane', 'konkord', 4000, 5),
('book', 'haary potter', 43, 6),
('computer', 'accer', 5000, 6),
('mobile', 'apple', 300, 7);
``
6. Выборка из таблиц _"пользователь - товар"_:
* ``select u.name as owner, p.type || ' "' || p.name || '"' as product from users as u left join products as p on u.id = p.owner;``
7. Запрос на изменение хозяина у некоторых продуктов:
* ``update products set owner = 1 where type like '%r' or id = 12;``
8. Удаление одного пользователя:
* ``delete from users where name = 'sonik';``
9. Посчитать и вывести каждого пользователя с кол-вом товара:
* ``select u.name, count(p) from users as u left join products as p on u.id = p.owner group by u.name;``
10. Добавить в таблицу users новое уникальное поле email:
* ``alter table users add column email varchar(20) unique;``
11. При следующих добавлениях в users id будет начинаться с 10000:
* ``select setval('users_id_seq', 1000);``
12. Добавить birthday в users. Значение даты не должно превышать текущую:
``alter table users add column birthday date check (birthday < now());``
13. Добавление поля age в users, которое будет автоматически залолняться при добавлении/обновлении записи из подсчета текущей даты и даты рождения пользователя
    * ``alter table users add column age int;``
    * ``create function count_age() returns trigger language plpgsql as $$ begin update users set age = date_part('year', age(now(), new.birthday)) where id = new.id; return new; end; $$;``
    * ``create trigger autoAge after insert or update of birthday on users for each row execute procedure count_age();``
14. Транзакции:
    * Неудачная:
        * ``begin``
        * ``insert into users(name, birthday) values('badman', '1001-01-02');``
        * ``insert into products(type) values('no owner');``
        * ``commit;``
    * Удачная:
        * ``begin``
        * ``insert into users(name, email, birthday, money) values('tim', 'tim@mailmail.gb', '1200-12-12', 13),
('gendalf', 'abracadabra@.comcom', '0001-01-01', 1000000);``
        * ``insert into products(type, name, price, owner) values('car', 'lexus', 1000, 1);``
        * ``commit``
15. Сохранение базы данных в sql файл:
* ``\q``
* ``pg_dump -O market > marketnika.sql``