--
-- PostgreSQL database dump
--

-- Dumped from database version 12.4 (Ubuntu 12.4-1.pgdg20.04+1)
-- Dumped by pg_dump version 12.4 (Ubuntu 12.4-1.pgdg20.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: count_age(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.count_age() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ begin update users set age = date_part('year', age(now(), new.birthday)) where id = new.id; return new; end; $$;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: products; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.products (
    id integer NOT NULL,
    type character varying(10),
    name character varying(20),
    price money,
    owner integer NOT NULL
);


--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.products_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.products_id_seq OWNED BY public.products.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(20),
    money money,
    email character varying(20),
    birthday date,
    age integer,
    CONSTRAINT users_birthday_check CHECK ((birthday < now()))
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: products id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.products ALTER COLUMN id SET DEFAULT nextval('public.products_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.products (id, type, name, price, owner) FROM stdin;
2	house	big	 40 000,00грн.	1
5	shop	product	 4 000,00грн.	3
6	cinema	3d	 5 000,00грн.	3
7	jeans	disel	 30,00грн.	3
8	bike	suzuki	 500,00грн.	3
13	book	haary potter	 43,00грн.	6
15	mobile	apple	 300,00грн.	7
1	car	ford	 30 000,00грн.	1
3	computer	hp	 300,00грн.	1
4	processor	intel	 400,00грн.	1
9	car	bmv	 54 400,00грн.	1
12	airplane	konkord	 4 000,00грн.	1
14	computer	accer	 5 000,00грн.	1
16	car	lexus	 1 000,00грн.	1
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.users (id, name, money, email, birthday, age) FROM stdin;
1	nikita	 1 400 000,00грн.	\N	\N	\N
2	ivan	 10 000,00грн.	\N	\N	\N
3	marta	 49 990,00грн.	\N	\N	\N
4	tom	 33 440,00грн.	\N	\N	\N
6	masha	 3 000,00грн.	\N	\N	\N
7	vika	 90 000,00грн.	\N	\N	\N
1001	tim	 13,00грн.	tim@mail.gb	1200-12-12	819
1002	gendalf	 1 000 000,00грн.	abracadabra@.com	0001-01-01	2019
\.


--
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.products_id_seq', 16, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.users_id_seq', 1002, true);


--
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: users users_email_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users autoage; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER autoage AFTER INSERT OR UPDATE OF birthday ON public.users FOR EACH ROW EXECUTE FUNCTION public.count_age();


--
-- Name: products products_owner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_owner_fkey FOREIGN KEY (owner) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

