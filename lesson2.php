<?php namespace l2;

require_once ("classes/Processor.php");
require_once ("classes/Ram.php");
require_once ("classes/User.php");


$bill = new \User("Bill");
$tom = new \User("Tom", 500);

$ram = new \Ram("Ram Samsung", 34.35, $bill, "ssd", 4);
$ram2 = new \Ram("Ram Intel", 12.3, $tom, "ssd", 4);
$processor = new \Processor("Processor Pentium", 35, $tom, 3);
$processor2 = new \Processor("Processor Tefal", 354, $bill, 8);


\Product::registerProduct($ram);
\Product::registerProduct($processor);

foreach (\Product::getIterator() as $p) {
    echo $p;
}

$ram4 = new \Ram("name", 34.4, $tom, "type", 4);


$r = new \ReflectionClass($ram4);
$o = null;
foreach($r->getProperties() as $p) {
    $o = $p->getName();
}
$prop = $r->getProperty($o);
$prop->setAccessible(true);

echo $prop->getValue($ram4);

?>