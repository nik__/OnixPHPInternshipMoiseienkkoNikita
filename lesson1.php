<?php namespace l1;

class User 
{
    private $name;
    private $balance;

    public function __construct($name, $balance=0)
    {
        $this->name = $name;
        $this->balance = $balance;    
    }

    public function printStatus()
    {
        echo "У пользователя {$this->name} сейчас на счету {$this->balance}.\n";
    }

    public function giveMoney($toUser, $money)
    {
        if($money > $this->balance) {
            echo "$this->name не имеет достаточных средств для проведения данной операции!\т";
            return;
        }
        $this->balance -= $money;
        $toUser->balance += $money;
        echo "Пользователь {$this->name} перечислил {$money} пользователю {$toUser->name}.\n";
    }
}
$bill = new User("Bill");
$tom = new User("Tom", 500);
$mario = new User("Mario", 390);

echo "{$bill->printStatus()}{$tom->printStatus()}{$mario->printStatus()}\n\n";

$bill->giveMoney($tom, 20);
$mario->giveMoney($tom, 20);
$tom->giveMoney($bill, 100);
$bill->giveMoney(new User("Nemo", 100000), 20);

echo "{$bill->printStatus()}{$tom->printStatus()}{$mario->printStatus()}\n\n";
?>