<?php
include_once 'console.php';

class User
{
    private $name;
    private $balance;

    public function __construct($name, $balance = 0)
    {
        $this->name = $name;
        $this->balance = $balance;
    }

    public function __toString()
    {
        return sprintf("%s%'.50s\n", "Пользователь", "{$this->name} с капиталом {$this->balance} грн.");
    }

    public function giveMoney($toUser, $money)
    {
        if ($money > $this->balance) {
            echo \Console::red("$this->name не имеет достаточных средств для проведения данной операции!\n");
            return false;
        }
        $this->balance -= $money;
        $toUser->balance += $money;
        echo \Console::green("Пользователь {$this->name} перечислил {$money} пользователю {$toUser->name}.\n");
        return true;
    }

    public function listProducts()
    {
        $result = [];
        foreach (Product::getIterator() as $product) {
            if ($product->getOwner() === $this) {
                $result[] = $product;
            }
        }
        return $result;
    }


    public function sellProduct(User $customer, Order $order=null)
    {
        //Проверяем наличие товара у продовца($this)
        if (count($this->listProducts()) === 0) {
            echo Console::red("К сожалению у  мистера {$this->name} ничего на продажу нет\n");
            return false;
        }
        //Проверяем, сформировал ли заказ($order) покупатель($customer)
        if(is_null($order)) { //если нет - пытаемся продать любой
            foreach($this->listProducts() as $product) { //проходим по всем товарам
                if($customer->giveMoney($this, $product->getPrice())) { //пробуем продать каждый, проверяя, есть ли у покупателя столько денег
                    $product->setOwner($customer); //в случае успеха меняем владельца товара с продавца на покупателя
                    $log = "Мистер {$customer->name}". " купил у ". $this->name. " ". $product->getName(). " за {$product->getPrice()} грн\n";
                    echo \Console::green($log);
                    return true; //выходим из метода при первой удачной продаже
                }
            } //если ничего не продали, выходим из метода
            echo \Console::red("Сделка не состоялась из-за нехватеи денег у мистера {$customer->name}\n");
            return false;
        }
        $isItem = false;//Поменяем на true, если у продавца будет подходящий товар
        //Если мы тут - это означает, что у продовца есть товар, а у покупателя конкретный заказ
        foreach($this->listProducts() as $product) { //Проходим по всем продуктам продавца
            if($order->isValidItem($product)) { //Если продукт совпадает с пожеланиями покупателя
                $isItem = true; //Поменяем флаг, что товар имеется(пригодится в будущем)
                if($customer->giveMoney($this, $product->getPrice())) { //Если у покупателя достаточно денег - продаём
                    echo \Console::green("{$this->name} успешно продал человеку по имени {$customer->name} {$product->getName()} за {$product->getPrice()} грн\n");
                    $product->setOwner($customer); //Меняем владельца продукта с продавца на покупателя
                    return true; //Выходим из метода
                }
            }
        } //Если мы тут - значит либо товара такого нет, либо не хватает денег, чтобы его купить
        if($isItem) { //Генерируем правильную причину провала сделки
            echo \Console::red("Товар у {$this->name} имеется, но к сожалению для {$customer->name}а это слишком дорого.\n");
        } else {
            echo \Console::red("Такого тавара как хочет покупатель у {$this->name} нет!\n");
        }
        return false;
    }
}