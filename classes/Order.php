<?php




class Order
{
    private $maxPrice;
    private $productFamily;
    private $otherRequests;

    public function __construct() { //Со старта создаётся заказ, которому соответствует любой товар
        $this->maxPrice = 1000000000;
        $this->productFamily = "any";
        $this->otherRequests = [];
    }

    public function price(float $max) {
        $this->maxPrice = $max;
        return $this;
    }

    public function family(string $instance) {
        $this->productFamily = $instance;
        return $this;
    }

    public function additional($additionalOptions) {
        $this->otherRequests = $additionalOptions;
        return $this;
    }

    public function refresh() {
        $this->additional([]);
        $this->price(1000000000);
        $this->family("any");
        return $this;
    }

    public function isValidItem(Product $product) { //Проверяем подходит ли продукт... Если что-то не так - конец проверки return false
        if($this->maxPrice >= $product->getPrice()) { //Если цена не дороже заказной
            //Если не указан конеретный тип товара ИЛИ если указан, то совпадает ли он с типом продукта
            if($this->productFamily === "any" || strcasecmp($this->productFamily, get_class($product)) === 0) {
                if(count($this->otherRequests) === 0) { //Если других пожеланий нет, то этот товар годен для продажи
                    return true;//Выходим из метода. Конец.
                }
                //Раз мы тут - значит имеются обобые пожелания для товара
                $reflect = new ReflectionClass($product);//Исследуем обьект товара
                $productProperties = [];//заготовка для массива всех его полей
                foreach($reflect->getProperties() as $pr) {
                    $productProperties[] = $pr->getName(); //получаем имя поля и сохраняем в наш массив
                }
                foreach($this->otherRequests as $key=>$value) { //работаем с каждым пожеланием покупателя
                    if(in_array($key, $productProperties)) {//есть ли у продукта такой параметр
                        $reflectProperty = $reflect->getProperty($key);
                        $reflectProperty->setAccessible(true);
                        $reflectValue = $reflectProperty->getValue($product);
                        if(!$reflectValue === $value) {
                            //Параметр продукта имеет другое значение
                            return false;
                        }
                    } else {
                        //У продукта нет специфичного параметра, поэтому он невалиден
                        return false;
                    }
                }
                //=================================================================
                return true;//Всё в продукте соответствует ожиданиям покупателя  ||
                //=================================================================
            } else {
                //Продукт не того типа, как хочет покупатель
                return false;
            }
        } else {
            //Продукт слишком дорог для покупателя
            return false;
        }
    }
}