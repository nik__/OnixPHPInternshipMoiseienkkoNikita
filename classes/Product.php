<?php


abstract class Product {
    private string $name;
    private float $price;
    private User $owner;

    private static $products = [];

    public function __construct($name, $price, $owner) {
        $this->name = $name;
        $this->price = $price;
        $this->owner = $owner;
        self::registerProduct($this);
    }

    public static function registerProduct(Product $product) {
        if(!in_array($product, self::$products)) {
            self::$products[] = $product;
        } else {
            echo "Такой продукт уже числится в регистре\n";
        }

    }

    public static function getIterator() {
        return new class(self::$products) implements \Iterator {
            private $array = [];
            private $posititon;
            function __construct($array) {
                $this->array = $array;
                $this->posititon = 0;
            }

            public function rewind() {
                $this->position = 0;
            }

            public function current() {
                return $this->array[$this->position];
            }

            public function key() {
                return $this->position;
            }

            public function next() {
                ++$this->position;
            }

            public function valid() {
                return isset($this->array[$this->position]);
            }
        };
    }

    public function getName(): string {
        return $this->name;
    }

    public function getPrice(): float {
        return $this->price;
    }

    public function getOwner(): User {
        return $this->owner;
    }

    public function setOwner($owner) {
        $this->owner = $owner;
    }
}