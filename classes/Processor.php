<?php
require_once ("Product.php");

class Processor extends Product {
    private float $frequency;

    public function __construct($name, $price, $owner, $frequency) {
        parent::__construct($name, $price, $owner);
        $this->frequency = $frequency;
    }

    public function __toString() {
        return sprintf("\n%'.-50s%s\n%'.-50s%s\n%'.-50s%s\n%s",
            "Процессор", "{$this->getName()}",
            "Частота", "$this->frequency Hz",
            "Цена", "{$this->getPrice()} грн",
            "{$this->getOwner()}");
    }
}

