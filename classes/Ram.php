<?php
require_once("Product.php");

class Ram extends Product {
    private string $type;
    private int $memory;

    public function __construct($name, $price, $owner, $type, $memory) {
        parent::__construct($name, $price, $owner);
        $this->type = $type;
        $this->memory = $memory;
    }

    public function getMemory() {
        return $this->memory;
    }

    public function __toString() {
        return sprintf("\n%'.-50s%s\n%'.-50s%s\n%'.-50s%s\n%'.-50s%s\n%s",
            "Оперативная память", "{$this->getName()}",
            "Тип", "{$this->type}",
            "Обьём", "{$this->memory} Gb",
            "Цена", "{$this->getPrice()} грн",
            "{$this->getOwner()}");
    }
}