<?php namespace l3;

require_once ('classes/User.php');
require_once ("classes/Processor.php");
require_once ("classes/Ram.php");
require_once ("classes/Order.php");
use \User as User;
use \Ram as Ram;
use \Processor as Processor;
use \Order as Order;


echo "\n========================>>> COЗДАЁМ ЛЮДЕЙ <<<=========================\n\n";
$vlad = new User("Влад");
$ivan = new User("Иван", 100);
$arshak = new User("Аршак", 200);
$maksim = new User("Максим", 300);
echo $vlad, $arshak, $ivan, $maksim;

echo "\n=========================>>> CОЗДАЁМ ТОВАР <<<=========================\n\n";
$ramVlada = new Ram("Samsung", 57.6, $vlad, "ssd", 8);
$processorVlada = new Processor("Intel", 89, $vlad, 2000);
$ramIvana1 = new Ram("Tefal", 4, $ivan, "hdd", 2);
$ramIvana2 = new Ram("Sony", 30.3, $ivan, "made in China", 100);
$processorArshaka = new Processor("Bosh", 44, $arshak, 1500);
foreach (Processor::getIterator() as $item) {
    echo $item;
}

echo "\n==========================>>> НАЧИНАЕМ ТОРГОВЛЮ <<<======================\n\n";
$do = "-----ДО----\n";
$posle = "---ПОСЛЕ---\n";
echo $do;
foreach([$ivan, $arshak, $vlad, $maksim] as $user) {
    echo "\n---------------------------------------------------------------------------\n\n$user\n\n";
    foreach($user->listProducts() as $p) {
        echo $p;
    }
}
//Эти сделки не состоятся
$maksim->sellProduct($ivan);
$arshak->sellProduct($vlad);
$order = new Order();
$vlad->sellProduct($arshak, $order->family("чайник"));
$vlad->sellProduct($arshak, $order->refresh()->family("ram")->price(2));
$vlad->sellProduct($arshak, $order->refresh()->family("processor")->price(150)->additional(["frequency"=>5000]));
//А эти состоятся
$vlad->sellProduct($arshak, $order->refresh()->additional(["frequency"=>2000]));
$arshak->sellProduct($maksim, $order);
$vlad->sellProduct($maksim);
$ivan->sellProduct($maksim, $order->refresh()->additional(["type"=>"made in China"]));
$arshak->sellProduct($ivan, $order->refresh()->price(200));
$ivan->sellProduct($maksim, $order->refresh()->additional(["memory"=>100]));
echo $posle;
foreach([$ivan, $arshak, $vlad, $maksim] as $user) {
    echo "\n---------------------------------------------------------------------------\n\n$user\n\n";
    foreach($user->listProducts() as $p) {
        echo $p;
    }
}
